$(function(){

    // Ici se placera tout notre code
	
/*==============================================================================================================================================================================
		Fonction #1 : attr()
		=> Permet de récupérer ou modifier l'attribut de l'élément sélectionné
		=> La fonction removeAttr() supprime l'attribut de l'élément sélectionné
		
		Fonction #2 : addClass()
		=> ajoute un attribut class à l'élément sélectionné
		=> removeClass() fait l'inverse
		
		Fonction #3 : html()
		=> récupère le contenu d'un élément HTML (= innerHTML() en JS pur)
		
		Fonction #4 : text()
		=> Retourne les données textuelles de l'élément sélectionné
		
		Fonction #5 : val()
		=> Permet de récupérer la valeur d'un élément et de la modifier
		=> Plutôt utilisée dans le contexte d'un formulaire
		
		LES PLUGINS
		
		En JavaScript, il est possible de développer des plugins qui sont appelés en tant que méthode sur les différents éléments d'un DOM.
		Un plugin peut avoir une infinité de paramètres. 
		
		
		LE THEMEROLLER
		
		= éditeur de thèmes mis à disposition par le site officiel de jQuery UI.
		
		Se décompose en plusieurs parties :
		
		1/ Les "font settings" :
			
			Permettent la modification des caractéristiques de la police d'un thème
			
		2/ Le "corner radius"
		
			Permet d'arrondir les angles des blocs conteneurs (ex : boutons).
			
		3/ Le "header" et la "toolbar"
		
			Propriétés CSS qui stylisent l'arrière-plan des en-têtes présents sur certains plugins. Le ThemeRoller offre un système de patterns transparents qui permettent
			d'ajouter un effet de texture (ex : dégradé, effet de brillance, quadrillage, ...). Les différentes données à rentrer sont, dans l'ordre :
			- couleur de l'arrière-plan (hexadecimal) ;
			- type de pattern ;
			- opacité.
			
		4/ Le "content"
			Propriétés CSS du contenu du DOM (border, text, icon).
			
		5/ Eléments cliquables
		
			Principalement les boutons, les onglets et les icônes. Trois niveaux pour styliser un élément cliquable : 
			- Style par défaut = "Clickable:default" dans ThemeRoller ;
			- Style hover = "Clickable:hover state" ;
			- Style active = "Clickable: active state".
			
			Configuration similaire aux header, content et toolbar (même propriétés).
			
		6/ Les "overlays"
			
			"Overlay" désigne l'élément qui recouvrira la page lorsqu'on ouvrira une nouvelle fenêtre. Généralement une texture répétée sur x et y pour former un AP semi-
			transparent.
			
			Section : "Modal Screen for Overlays" (définition de l'opacité de l'overlay)
			
		7/ Les "shadows"
		
			= Ombres.
			
			Section : "Drop Shadows" (premiers paramètres identiques aux parties précédentes) + propriétés particulières :
			- Shadow Thickness : étendue de l'ombre ;
			- Top/Left offset : décalages de l'ombre par rapport au bloc mis en avant ;
			- Corners : arrondis de l'ombre.
			
			
		LE DRAG'N'DROP
		
		A/ Le "drag"
			=> Utilise un système de coordonnées (x et y). Des propriétés comme "mousedown", "mousemove", "mouseup" permettent la connaissance des coordonnées du curseur et
				les transmettre à l'élément déplacé jusqu'à ce que l'utilisateur le "lâche". 
				
		B/ Le "drop"
			=> Lorsque l'élément en "drag" est lâché, on détruit tous les événements associés, ce qui annule la "transmission" des coordonnées et immobilise celui-ci. 
			
		C/ Plugin : Draggable
			a) Par défaut :
				$("#element").draggable();
				
			b) Approfondi :
				-> Limiter le déplacement :
					2 façons :
						<=> Restriction au niveau de l'axe (x ou y) :
								- Restriction au niveau horizontal								
									$("#drag").draggable({
										axis : 'x'
									}
								
								- Restriction au niveau vertical 
									$("#drag").draggable({
										axis : 'y'
									}
						<=> Spécification d'une zone restrictive
							$("#drag").draggable({
								containment : "#limitation"
							}
							
				-> Définition d'un motif :
					2 façons :
						<=> Modification du paramètre "snap" : Prend en valeur la classe ou l'identifiant du bloc exerçant le magnétisme ;
						<=> Modification du paramètre "grid" : Prend en valeur un tableau contenant la longueur en pixels d'une unité sur x, puis sur y.
						
						
		SELECTION ET REDIMENSION
		
		=> REDIMENSION
		+) Préserver le ratio (proportions)
			$('#resize').resizable({
				aspectRatio : true // respect du ratio
			});
		
		++) Extremum des propriétés
			$('#resize').resizable({
				minWidth : 50,
				maxWidth : 300,
				minHeight : 50,
				maxHeight : 300
			});
			
		+++) Limitation dans un bloc parent
			$('#resize').resizable({
				containment : '#limites'
			});
			
		++++) Ajoutons un peu d'animation
			$('#resize').resizable({
				animate : true
			});
		
		=> SELECTION
		+) A la fin d'une sélection
			$('#selection').selectable({
				stop : function(){
					// code à exécuter
				}
			});
			
		ORGANISATION DE LA PAGE WEB
		
		A) Limiter le déplacement
			Utilisation des paramètres axis (x ou y) et containment (élément du DOM ou mot-clé "parent")
		
		B) Connecter deux listes
			$('.premiere').sortable({
				connectWith : '.seconde'
			});

			$('.seconde').sortable({
				connectWith : '.premiere'
			});
			
		C) Modifier le curseur
			
		
=============================================================================================================================================================================*/
	$('#task1').draggable({
		containment : "#body", //Le bloc ne sera déplaçable que 
		snap: true,
		grid : [10 , 10]
	});
	
	$('#task2').draggable({
		containment : "#body",
		revert:'valid',
		grid : [10 , 10]
	});
	
	$('#task3').draggable({
		containment : "#body",
		grid : [10 , 10]
	});
	
	//collapse1 
	$('#collapse1').droppable({
		accept: '#task3', //Dans ce cas, l'alert ne sera affichée que lorsqu'on déplacera le bloc "task3"
		drop : function(){
			alert('Tu lâches ça maintenant, viens on va manger !');
		}
	});
	
	$('#res').resizable({
		helper : 'help' // je donne la classe stylisant mon helper
	});
	
	$('.objet').draggable({ 
		revert : 'invalid', // l'élément est renvoyé à sa place s'il n'est pas déposé dans la bonne zone
		cursor : 'move' // on modifie le curseur pour faciliter la compréhension du visiteur
	});

	$('#panier').droppable({
		accept : '.objet',
		drop : function(event, ui){ // les deux arguments à spécifier (l'un définit l'event courant, l'autre l'élément courant)
			var current = ui.draggable; // on entre l'élément courant dans une variable
			var resultat = $('.resultat');

			current.fadeOut(); // on fait disparaître notre élément
			resultat.append('<li class="obj-panier">' + current.html() + '</li>'); // on ajoute dans le panier et on définit une classe à chaque élément
		}
	});

	$('.resultat').selectable({
		stop : function(){

			$('.ui-selected', this).each(function(){ // on traite chaque élément ayant été sélectionné (this est facultatif, mais permet d'éviter des conflits)

			$('#articles').append('<li class="objet">' + $(this).text() + '</li>'); // on ajoute le contenu de l'article dans le magasin
			$(this).remove(); // on le supprime ensuite du panier

			$('.objet').draggable({ // et on veille à redéfinir le plugin !
				revert : 'invalid', 
				cursor : 'move'
			});

		});

		}
	});
	
	$('.premiere').sortable({
		connectWith : 'ul',
		cursor:'move',
		cursorAt : { left : 0 },
		change : function(){
			//alert('Echange en cours !');
		},
		dropOnEmpty:true
	});

	$('.seconde').sortable({
		cursor:'move',
		connectWith : '.premiere',
		receive : function(){
			alert('Je confirme');
		}
	});
	
	$('#accordeon').accordion();
});